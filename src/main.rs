fn main() {
    let input = include_str!("../input.txt");
    println!("solution A: {}", solve_for_a(input));
    println!("solution B: {}", solve_for_b(input));
}

#[test]
fn test_solve_for_a() {
    let test_input = include_str!("../test.txt");
    assert_eq!(solve_for_a(test_input), 2);
}

// in how many assignment pairs does one range fully contain another?
fn solve_for_a(input: &str) -> u32 {
    let lines = input.lines();
    let mut overlap_count = 0;

    for line in lines {
        let mut terminals: [u32; 4] = [0; 4];
        line.split(',').enumerate().for_each(|(i, range)| {
            range.split('-').enumerate().for_each(|(j, terminal)| {
                let idx = i * 2 + j;
                terminals[idx] = terminal.parse().unwrap();
            });
        });

        let range_a = terminals[0]..=terminals[1];
        let range_b = terminals[2]..=terminals[3];

        if range_a.contains(&terminals[2]) && range_a.contains(&terminals[3])
            || range_b.contains(&terminals[0]) && range_b.contains(&terminals[1])
        {
            overlap_count += 1;
        }
    }

    overlap_count
}

#[test]
fn test_solve_for_b() {
    let test_input = include_str!("../test.txt");
    assert_eq!(solve_for_b(test_input), 4);
}

// in how many assignment pairs is there any overlap at all?
fn solve_for_b(input: &str) -> u32 {
    let lines = input.lines();
    let mut overlap_count = 0;
    for line in lines {
        let mut terminals: [u32; 4] = [0; 4];
        line.split(',').enumerate().for_each(|(i, range)| {
            range.split('-').enumerate().for_each(|(j, terminal)| {
                let idx = i * 2 + j;
                terminals[idx] = terminal.parse().unwrap();
            });
        });

        let range_a = terminals[0]..=terminals[1];
        let range_b = terminals[2]..=terminals[3];

        if range_a.contains(&terminals[2])
            || range_a.contains(&terminals[3])
            || range_b.contains(&terminals[0])
            || range_b.contains(&terminals[1])
        {
            overlap_count += 1;
        }
    }

    overlap_count
}
